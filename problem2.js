function problem(last_entry){
    last_entry = last_entry[last_entry.length - 1];
    return "Last car is a " + last_entry["car_make"] + " " + last_entry["car_model"]
}

module.exports = problem;