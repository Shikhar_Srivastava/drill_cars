function year(allYear){
    let allCarYear = [];
    for (let i=0; i < allYear.length; i++) {
        allCarYear.push(allYear[i]["car_year"]);
        
    }
    let carsBefore2000 = [];
    for (let i = 0; i < allCarYear.sort().length; i++) {
        if (allCarYear[i] < 2000) {
            carsBefore2000.push(allCarYear[i]);
        }
    }
    return carsBefore2000;
}


module.exports = year;